package pt.ipp.isep.dei.examples.tdd.basic.domain;

import java.awt.print.Book;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

public class AdressDirectory {
    HashMap<String, Bookmark> bookmarks = new HashMap<String, Bookmark>();

    public int createBookmark(String url){
        try{
            URL uri = new URL(url);
            Bookmark mark = bookmarks.get(url);
            if(mark == null){
                mark = new Bookmark();
                bookmarks.put(url, mark);
            }else{
                mark.increaseRating();
                bookmarks.put(url, mark);
            }
            return 0;
        }catch(MalformedURLException e){
            return -99;
        }
    }

    public int tagURL(String url, String tag){
        Bookmark mark = bookmarks.get(url);
        if(mark != null){
            return mark.tagBookmark(tag);
        }else{
            return -99;
        }
    }

    public ArrayList<String> findSecureURLs(){
        ArrayList<String> secureURLs = new ArrayList<>();
        Iterator it = bookmarks.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            if(pair.getKey().toString().startsWith("https")){
                secureURLs.add(pair.getKey().toString());
            }
        }
        return secureURLs;
    }

    public ArrayList<String> filterURL(String tag){
        ArrayList<String> filtered = new ArrayList<>();
        Iterator it = bookmarks.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            Bookmark bookmark = (Bookmark) pair.getValue();
            if(bookmark.getTags().contains(tag)){
                filtered.add(pair.getKey().toString());
            }
        }
        return filtered;
    }

    public ArrayList<String> filterURL2(ArrayList<String> tags){
        ArrayList<String> filtered = new ArrayList<>();
        for (String tag: tags) {
            for(String results: filterURL(tag)){
                filtered.add(results);
            }
        }
        return filtered;
    }

    public int removeTag(String url, String tag){
        Bookmark mark = bookmarks.get(url);
        if(mark != null){
            return mark.rmTagBookmark(tag);
        }else{
            return -99;
        }
    }

    public int removeBookmark(String url){
        if(bookmarks.remove(url) == null){
            return -99;
        }else {
            return 99;
        }
    }




}
